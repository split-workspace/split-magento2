/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'mage/url',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Split_SplitPaymentGateway/js/action/split-redirect-action',
        'Magento_Ui/js/model/messageList',
        'Magento_Customer/js/customer-data',
    ],
    function ($, mageUrl, Component, additionalValidators, splitRedirectAction, globalMessageList, customerData) {
        'use strict';

        return Component.extend({
            defaults: {
                redirectAfterPlaceOrder: false,
                template: 'Split_SplitPaymentGateway/payment/form'
                // transactionResult: ''
            },

            initObservable: function () {

                this._super()
                    .observe([
                        'transactionResult'
                    ]);
                return this;
            },

            getCode: function() {
                return 'split_gateway';
            },

            getData: function() {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'transaction_result': this.transactionResult()
                    }
                };
            },

            getInfo: function () {
                return window.checkoutConfig.payment['split_gateway'].info
            },

            getSplitLogo: function () {
                return window.checkoutConfig.payment['split_gateway'].logoSrc;
            },

            getVisibleType: function() {
                return window.checkoutConfig.payment['split_gateway'].visibleType;
            },

            getTransactionResults: function() {
                return _.map(window.checkoutConfig.payment.split_gateway.transactionResults, function(value, key) {
                    return {
                        'value': key,
                        'transaction_result': value
                    }
                });
            },

            splitGatewayCheckout : function () {
                if (additionalValidators.validate()) {

                    var url = mageUrl.build("split/payment/splitCheckout")

                    var email = window.checkoutConfig.customerData.email;

                    if (!window.checkoutConfig.quoteData.customer_id) {
                        email = document.getElementById("customer-email").value;
                    }

                    var data = $("#co-shipping-form").serialize();
                    data = data + '&email=' + email;
                    
                    $.ajax({
                        url: url,
                        method: 'post',
                        data: data
                    }).done(function (response) {
                        splitRedirectAction(response.redirect_url)
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        globalMessageList.addErrorMessage({
                            'message': 'http status: ' + textStatus + ' error: ' + errorThrown + ' ----- ' + 'page will be reloaded'
                        });
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);

                        $('body').trigger('processStop');
                    }).always(function () {
                        customerData.invalidate(['cart']);
                    });
                } else {
                    globalMessageList.addErrorMessage({'message': 'Please handle all validation problems before checking out with Split.'});
                }
            },
        });
    }
);