define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                // type: 'split-gateway',
                type: 'split_gateway',
                component: 'Split_SplitPaymentGateway/js/view/payment/method-renderer/split_gateway'
                // component: 'Split_SplitPaymentGateway/payment/method-renderer/split_Gateway'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
