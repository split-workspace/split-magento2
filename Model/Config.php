<?php

namespace Split\SplitPaymentGateway\Model;

use Magento\Payment\Model\Method\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Directory\Model\Currency;
use Magento\Tax\Model\Config as TaxConfig;
use Psr\Log\LoggerInterface as Logger;

class Config implements ConfigInterface
{
    const KEY_ACTIVE = 'active';
    const API_URL_SANDBOX = 'https://sandbox.paywithsplit.co';
    const API_URL_PRODUCTION = 'https://app.paywithsplit.co';

    const API_KEY_SANDBOX = 'api_key_sandbox';
    const API_KEY_PRODUCTION = 'api_key_production';

    const SECRET_KEY_SANDBOX = 'secret_key_sandbox';
    const SECRET_KEY_PRODUCTION = 'secret_key_production';


    protected $_logger;
    protected $methodCode = 'split_gateway';
    protected $scopeConfig;
    protected $storeManager;
    protected $pathPattern;
    protected $currency;
    protected $taxConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Currency $currency,
        TaxConfig $taxConfig,
        Logger $logger
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->currency = $currency;
        $this->taxConfig = $taxConfig;
        $this->_logger = $logger;
    }

    public function setMethodCode($methodCode)
    {
        $this->methodCode = $methodCode;
    }

    public function setPathPattern($pathPattern)
    {
        $this->pathPattern = $pathPattern;
    }

    public function getApiKey()
    {
        return ($this->getValue('sandbox_mode')) ?
            $this->getValue(self::API_KEY_SANDBOX) :
            $this->getValue(self::API_KEY_PRODUCTION);
    }

    public function getSecretKey()
    {
        return ($this->getValue('sandbox_mode')) ?
            $this->getValue(self::SECRET_KEY_SANDBOX) :
            $this->getValue(self::SECRET_KEY_PRODUCTION);
    }

    public function getPrivateApiKey()
    {
        return ($this->getValue('sandbox_mode')) ?
            $this->getValue(self::KEY_PRIVATE_KEY_SANDBOX) :
            $this->getValue(self::KEY_PRIVATE_KEY_PRODUCTION);
    }

    public function getPublicApiKey()
    {
        return ($this->getValue('sandbox_mode')) ?
            $this->getValue(self::KEY_PUBLIC_KEY_SANDBOX) :
            $this->getValue(self::KEY_PUBLIC_KEY_PRODUCTION);
    }

    public function getApiUrl()
    {
        return ($this->getValue('sandbox_mode')) ?
            self::API_URL_SANDBOX :
            self::API_URL_PRODUCTION;
    }

    protected function _getSpecificConfigPath($fieldName)
    {
        if ($this->pathPattern) {
            return sprintf($this->pathPattern, $this->methodCode, $fieldName);
        }

        return "payment/{$this->methodCode}/{$fieldName}";
    }

    public function getValue($key, $storeId = null)
    {
        $this->_logger->debug('key: ' . $key);
        $value = $this->scopeConfig->getValue('payment/split_gateway/' . $key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $this->_logger->debug('returning value: ' . $value);
        return $value;
    }
}
