<?php
namespace Split\SplitPaymentGateway\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Gateway\ConfigInterface;

/**
 * Class ConfigProvider
 */
final class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'split_gateway';
    const SUCCESS = 0;
    const FRAUD = 1;

    /**
     * Injected config object
     *
     * @var \Magento\Payment\Gateway\ConfigInterface
     */
    protected $config;

    public function __construct(
        ConfigInterface $config
    ) {
        $this->config = $config;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'transactionResults' => [
                        self::SUCCESS => __('Success'),
                        self::FRAUD => __('Fraud')
                    ],
                    'info' => $this->config->getValue('info'),
                    'logoSrc' => $this->config->getValue('icon'),
                    'splitApiKey' => $this->config->getValue('apiKey'),
                    'splitSecretKey' => $this->config->getValue('secretKey')
                ]
            ]
        ];
    }
}
