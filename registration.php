<?php
/**
 *
 * @author Split
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Split_SplitPaymentGateway',
    __DIR__
);
