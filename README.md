# Using Split Payment Gateway on Magento

Split is a payment gateway solution that allows you to charge your customers in interest-free instalments, available as an extension package for Magento 2.

If you at any point during installation encounter any problems, please reach out to Split merchant support at partners@paywithsplit.co.

## Install Using Composer (Recommended)
For the below steps, Composer needs to be installed on the server running your Magento installation.

* Open a CLI on your server and navigate to your Magento installation directory.
* Run `composer require paywithsplit/magento-2-split`
* If prompted, input your Magento Marketplace credentials (Found by signing in to marketplace.magento.com -> “My Profile” in top-right corner -> Access Keys -> Create New Access Key if needed -> copy Public Key as username, Private Key as password)
* Run `php bin/magento setup:di:compile`
* Run `php bin/magento setup:upgrade `
* Run `php bin/magento setup:static-content:deploy -f`
* Run `php bin/magento cache:clean`

## Install Without Composer

* Download the Split repository as a .zip file from BitBucket
* On your server, create a new folder structure under your Magento installation directory: [MAGENTO INSTALLATION DIR]/app/code/Split/SplitPaymentGateway
* Unzip the contents of the downloaded Split repository to the newly created SplitPaymentGateway folder.
* From CLI, navigate to your Magento installation directory and run the following commands:
* Run `php bin/magento setup:di:compile`
* Run `php bin/magento setup:upgrade`
* Run `php bin/magento setup:static-content:deploy -f`
* Run `php bin/magento cache:clean`

## After Installation
* To access the Split extension settings, go to your Magento admin dashboard -> Stores -> Configuration -> Sales -> Payment Gateways -> Under “Other Payment Methods” next to the Split logo, click “Configure”
* Enter API Key + Secret Keys as given to you by Split and press “Save Config” in the top right.
* To test Split purchases, you can toggle “Sandbox Mode”  yes/no through the dropdown in the settings. 
  When “Sandbox Mode” is set to “yes”, purchases can be made without spending real money. For credit/debig card, use card number `4242 4242 4242 4242` + any CVV/expiry date
  When you are ready to use the plugin live, set “Sandbox Mode” to “no” and save settings. You are now ready to accept Split payments!
