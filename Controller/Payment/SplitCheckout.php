<?php

namespace Split\SplitPaymentGateway\Controller\Payment;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ConfigInterface;
use Split\SplitPaymentGateway\Model\Config as SplitConfig;
use Magento\Framework\Controller\Result\JsonFactory as JsonResultFactory;
use \Magento\Checkout\Model\Session as CheckoutSession;
use Psr\Log\LoggerInterface as Logger;
use \Magento\Framework\Encryption\EncryptorInterface as Encryptor;
use \Magento\Store\Model\StoreManagerInterface as StoreManager;
use \Magento\Quote\Model\ResourceModel\Quote as QuoteRepository;

/**
 * Class SplitCheckout
 * @package Split\SplitPaymentGateway\Controller\Payment
 */
class SplitCheckout extends Action
{
    /**
     * @var Context
     */
    private $context;
    /**
     * @var JsonResultFactory
     */
    private $jsonResultFactory;

    protected $_encryptor;
    protected $_logger;
    protected $_checkoutSession;
    protected $_storeManager;
    protected $_quoteRepository;
    
    /**
     * @var SplitConfig
     */
    private $splitConfig;

    public function __construct(
        Context $context,
        JsonResultFactory $jsonResultFactory,
        SplitConfig $splitConfig,
        Logger $logger,
        Encryptor $encryptor,
        CheckoutSession $checkoutSession,
        QuoteRepository $quoteRepository,
        StoreManager $storeManager
    ) {
        parent::__construct($context);
        $this->context = $context;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->splitConfig = $splitConfig;
        $this->_logger = $logger;
        $this->_encryptor = $encryptor;
        $this->_checkoutSession = $checkoutSession;
        $this->_quoteRepository = $quoteRepository;
        $this->_storeManager = $storeManager;
    }

    public function execute()
    {
        try {

            $data = $this->_checkoutSession->getData();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 

            // Get Quote
            $quote = $cart->getQuote();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->get('Magento\Customer\Model\Session');
            $customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');

            if ($customerSession->isLoggedIn()) {
                $customerId = $customerSession->getCustomer()->getId();
                $customer = $customerRepository->getById($customerId);
    
                // customer login
                $quote->setCustomer($customer);
     
            } else {
                $post = $this->getRequest()->getPostValue();

                if (!empty($post['email'])) {
                    $email = htmlspecialchars($post['email'], ENT_QUOTES);
                    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
                    try {
                        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $quote->setCustomerEmail($email)
                                ->setCustomerIsGuest(true)
                                ->setCustomerGroupId(\Magento\Customer\Api\Data\GroupInterface::NOT_LOGGED_IN_ID);
                        }
                    } catch (\Exception $e) {
                        $result = $this->jsonResultFactory->create()->setData(
                            ['error' => 1, 'message' => $e->getMessage()]
                        );
                        return $result;
                    }
                }
            }

            // Get order ID
            $quote->reserveOrderId();

            $this->_quoteRepository->save($quote);
            $this->_checkoutSession->replaceQuote($quote);
            
            $orderId = $quote->getReservedOrderId();
            $actualOrderId = $quote->getId();

            // Get amount
            $grandTotal = $quote->getBaseGrandTotal();

            // Get billing details
            $billingAddress = $quote->getBillingAddress();
            $billingAddressData = $billingAddress->getdata();

            // Get shipping details
            $shippingAddress = $quote->getShippingAddress();
            $shippingAddressData = $shippingAddress->getdata();

            $name = $billingAddressData['firstname'] . $billingAddressData['middlename'] . $billingAddressData['lastname'];
            $email = $billingAddressData['email'];
            $phone = $billingAddressData['telephone'];
            

            // Get API Key
            $apiKey = $this->splitConfig->getApiKey();

            // Get Secret Key
            $secretKey = $this->splitConfig->getSecretKey();

            // Get currency
            $currency = $this->_storeManager->getStore()->getBaseCurrency()->getCode();

            // Build signature
            $signaturePayload = [
                'apiKey' => $apiKey,
                'currency' => $currency,
                'orderId' => $orderId
            ];

            $sourceString = $apiKey . ' ' . $currency . ' ' . $orderId;
            $signature = hash_hmac('sha256', $sourceString , $secretKey);

            // Build Extra Info
            $extra_info = array();

            $extra_info['actualOrderId'] = $actualOrderId;

            // Shipping
            $shipping_data = array();
            $shipping_data['address_1'] = $shippingAddressData['street'];
            $shipping_data['postcode'] = $shippingAddressData['postcode'];
            $shipping_data['country'] = $shippingAddressData['country_id'];
            $shipping_data['city'] = $shippingAddressData['city'];

            $extra_info['shipping'] = $shipping_data;
            
            // Billing
            $billing_data = array();
            $billing_data['address_1'] = $billingAddressData['street'];
            $billing_data['postcode'] = $billingAddressData['postcode'];
            $billing_data['country'] = $billingAddressData['country_id'];
            $billing_data['city'] = $billingAddressData['city'];

            $extra_info['billing'] = $billing_data;

            $extra_info_json = json_encode($extra_info);
            $json_base64 = base64_encode($extra_info_json);

            $baseUrl = $this->splitConfig->getApiUrl();

            $redirectUrl = 
                $baseUrl . '/process/welcome' .
                '?apiKey=' . $apiKey .
                '&orderId=' . $orderId .  
                '&price=' . $grandTotal . 
                '&sig=' . $signature .
                '&title=MagentoOrder' . 
                '&name=' . $name .
                '&email=' . $email .
                '&currency=' . $currency .
                '&phone=' . $phone .
                '&extraInfo=' . $json_base64 .
                '&platformType=4';

            $jsonData = ['success' => true, 'redirect_url' => $redirectUrl];
        } catch (Exception $e) {
            $jsonData = ['success' => false, 'message' => $e->getMessage()];
        }

        $jsonResult = $this->jsonResultFactory->create()->setData(
            $jsonData
        );

        return $jsonResult;
    }
}
