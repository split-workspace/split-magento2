<?php

namespace Split\SplitPaymentGateway\Controller\Payment;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Psr\Log\LoggerInterface as Logger;
use Magento\Checkout\Model\Session;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Quote\Model\QuoteManagement;
use Magento\Sales\Model\OrderRepository;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Sales\Model\Order\Payment\Repository as PaymentRepository;
use Magento\Sales\Model\Order\Payment\Transaction\Repository as TransactionRepository;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface as TransactionBuilder;
use \Magento\Sales\Model\Service\InvoiceService as InvoiceService;
use \Magento\Framework\DB\TransactionFactory as TransactionFactory;
use Split\SplitPaymentGateway\Model\Config as SplitConfig;
use \Magento\Framework\Controller\Result\JsonFactory;

/**
 * 
 * Payment cancel action
 *
 * @package Astound\Affirm\Controller\Payment
 */
class Confirm extends Action implements CsrfAwareActionInterface
{
    protected $_logger;
    protected $checkoutSession;
    protected $checkout;
    protected $_quoteManagement;
    protected $_quoteRepository;
    protected $_paymentRepository;
    protected $_transactionBuilder;
    protected $_invoiceService;
    protected $_transactionFactory;
    protected $splitConfig;
    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        return null;
    }
 
    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }   

    public function __construct(
        Context $context,
        Logger $logger,
        Session $checkoutSession,
        CartManagementInterface $quoteManager,
        CartRepositoryInterface $quoteRepository,
        PaymentRepository $paymentRepository,
        OrderRepository $orderRepository,
        TransactionRepository $transactionRepository,
        TransactionBuilder $transactionBuilder,
        InvoiceService $invoiceService,
        TransactionFactory $transactionFactory,
        SplitConfig $splitConfig,
        JsonFactory $jsonFactory
    ){
        $this->_logger = $logger;
        $this->checkoutSession = $checkoutSession;
        $this->_quoteManagement = $quoteManager;
        $this->_paymentRepository = $paymentRepository;
        $this->_orderRepository = $orderRepository;
        $this->_quoteRepository = $quoteRepository;
        $this->_transactionRepository = $transactionRepository;
        $this->_transactionBuilder = $transactionBuilder;
        $this->_invoiceService = $invoiceService;
        $this->_transactionFactory = $transactionFactory;
        $this->splitConfig = $splitConfig;
        $this->jsonFactory = $jsonFactory;

        parent::__construct($context);
    }

    public function execute()
    {

        $query = json_decode($this->getRequest()->getContent(), true);

        $this->_logger->debug('Split confirm endpoint received Query:');
        $this->_logger->debug(print_r($query, true));

        $sourceString = $query['apiKey'] . ' ' . $query['currency'] . ' ' . $query['incrementId'];
        
        $this->_logger->debug('Source String: ' . $sourceString);
        $secretKey = $this->splitConfig->getSecretKey();

        $signature = hash_hmac('sha256', $sourceString , $secretKey);

        $this->_logger->debug('Generated signature: ' . $signature);
        $this->_logger->debug('Signature in query: ' . $query['signature']);

        if ($signature != $query['signature']) {
            throw new \InvalidArgumentException('Wrong signature in payload.');
            exit;
        }

        // $quote = $this->checkoutSession->getQuote();
        $quote = $this->_quoteRepository->get($query['orderId']);

        // $quote->reserveOrderId();

        // prepare session to success or cancellation page
        $this->checkoutSession->clearHelperData();

        // "last successful quote"
        $quoteId = $quote->getId();
        $this->checkoutSession->setLastQuoteId($quoteId)->setLastSuccessQuoteId($quoteId);

        $customerEmailAddress = $quote->getCustomerEmail();
        
        $quote->collectTotals();
        
        // Restore Customer email address if it becomes null/blank
        if(empty($quote->getCustomerEmail())){

            if ($customerEmailAddress) {
                $quote->setCustomerEmail($customerEmailAddress);
            } else {
                $quote->setCustomerEmail($quote->getBillingAddress()->getEmail());
            }
        }

        // Convert quote to order
        $order = $this->_quoteManagement->submit($quote);
        
        $orderId = $order->getId();
        $payment = $order->getPayment();
        $this->_createTransaction($order, $orderId , $payment); 
        
        $resultJson = $this->jsonFactory->create();
        return $resultJson->setData(['success' => $orderId]);
    }

    private function _createTransaction($order = null, $orderId = null, $payment=null)
    {
        try {
            $payment->setLastTransId($orderId);
            $payment->setTransactionId($orderId);
            $formatedPrice = $order->getBaseCurrency()->formatTxt(
                $order->getGrandTotal()
            );

            $message = __('The authorized amount is %1.', $formatedPrice);
            //get the object of builder class
            $trans = $this->_transactionBuilder;
            $transaction = $trans->setPayment($payment)
                ->setOrder($order)
                ->setTransactionId($orderId)
                ->setFailSafe(true)
                //build method creates the transaction and returns the object
                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->addTransactionCommentsToOrder(
                $transaction,
                $message
            );
            $payment->setIsTransactionApproved(true);
            $payment->setParentTransactionId(null);

            $this->_paymentRepository->save($payment);
            
            $order->setBaseCustomerBalanceInvoiced(null);
            $order->setCustomerBalanceInvoiced(null);

            // prepare invoice and generate it
            $invoice = $this->_invoiceService->prepareInvoice($order);
            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE); // set to be capture offline because the capture has been done previously
            $invoice->register();

            /** @var \Magento\Framework\DB\Transaction $transaction */
            $dbTransaction = $this->_transactionFactory->create();
            $dbTransaction->addObject($order)
                ->addObject($invoice)
                ->addObject($invoice->getOrder())->save();

            $this->_orderRepository->save($order);

            $this->_transactionRepository->save($transaction);

            return  $transaction->getTransactionId(); 
        } catch (\Exception $e) {
            //log errors here
            $this->_logger->debug("Transaction Exception: There was a problem with creating the transaction. ".$e->getMessage());
        }
    }
}
