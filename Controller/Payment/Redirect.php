<?php

namespace Split\SplitPaymentGateway\Controller\Payment;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Psr\Log\LoggerInterface as Logger;
use Magento\Checkout\Model\Session;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Sales\Model\OrderRepository;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use \Magento\Framework\Api\SearchCriteriaBuilder as SearchCriteraBuilder;

/**
 * 
 * Payment cancel action
 *
 * @package Astound\Affirm\Controller\Payment
 */
class Redirect extends Action
{
    protected $_logger;
    protected $checkoutSession;
    protected $resultRedirect;
    protected $searchCriteraBuilder;
 
    public function __construct(
        Context $context,
        Logger $logger,
        Session $checkoutSession,
        OrderRepository $orderRepository,
        SearchCriteraBuilder $searchCriteraBuilder
    ){
        $this->_logger = $logger;
        $this->orderRepository = $orderRepository;
        $this->checkoutSession = $checkoutSession;
        $this->searchCriteriaBuilder = $searchCriteraBuilder;

        parent::__construct($context);
    }

    public function execute()
    {
        // Todo: Add signature validation here
        $query = json_decode($this->getRequest()->getContent(), true);

        $queryOrderId = (int) $this->getRequest()->getParam('orderId');

        $this->checkoutSession->clearHelperData();
        
        $order = $this->orderRepository->get($queryOrderId);
        
        $quoteId = $order->getQuoteId();
        $this->checkoutSession->setLastQuoteId($quoteId)->setLastSuccessQuoteId($quoteId);

        if ($order) {
            $this->checkoutSession->setLastOrderId($order->getId())
                ->setLastRealOrderId($order->getIncrementId())
                ->setLastOrderStatus($order->getStatus());
        }
        
        $this->_redirect('checkout/onepage/success');
        return;
    }
}
